package com.namnoit.awesometext

import android.content.Context
import android.graphics.*
import android.text.*
import android.text.style.AbsoluteSizeSpan
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.annotation.IntDef
import androidx.appcompat.widget.AppCompatEditText

const val MIN_SIZE = 100
const val MAX_SIZE = 500
const val TYPE_WRITER_SIZE = 25F
private const val TAG = "TEXT"

class AwesomeEditText : AppCompatEditText {
    companion object {
        @IntDef(BACKGROUND_MODE_FILLED, BACKGROUND_MODE_TRANSPARENT, BACKGROUND_MODE_NONE)
        @Retention(AnnotationRetention.SOURCE)
        annotation class BackgroundMode

        const val BACKGROUND_MODE_FILLED = 1
        const val BACKGROUND_MODE_TRANSPARENT = 2
        const val BACKGROUND_MODE_STROKE = 3
        const val BACKGROUND_MODE_NONE = 0

        @IntDef(TYPE_MODE_MODERN, TYPE_MODE_STRONG, TYPE_MODE_TYPE_WRITER, TYPE_MODE_CLASSIC)
        @Retention(AnnotationRetention.SOURCE)
        annotation class TypeMode

        const val TYPE_MODE_MODERN = 1
        const val TYPE_MODE_SAIGON = 2
        const val TYPE_MODE_STRONG = 3
        const val TYPE_MODE_TYPE_WRITER = 4
        const val TYPE_MODE_CLASSIC = 5
    }

    var backgroundPaint = Paint()
    private lateinit var linesBoundary: LinesBoundary
    var path = Path()
    private var mainColor = Color.BLUE
    private var padding = 10f
    private var corner = 2 * padding
    var keyboardHeight = 0
        set(value) {
            field = value
            availableHeight = screenSize.y - keyboardHeight - topHeight
            translationY = -keyboardHeight / 2f  + topHeight/2
            verticalScale()
            calculateBoundary()
        }
    var topHeight = 0
    private var screenSize = Point()
    private var availableHeight = 0
    private var isVerticalScaled = true
    var scaleInClassicMode = 1f
        set(value) {
            field = value
            scale()
            calculateBoundary()
        }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defaultStyleAttributes: Int) :
            super(context, attributeSet, defaultStyleAttributes) {
        backgroundPaint.strokeWidth = 5f
        typeface = Typeface.createFromAsset(context.assets, "fonts/modern.otf")
        val windowsManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowsManager.defaultDisplay
        display.getSize(screenSize)
    }

    @TypeMode
    var typeMode = TYPE_MODE_MODERN
        set(value) {
            field = value
            removeAutoSizeSpan()
            when (field) {
                TYPE_MODE_MODERN -> {
                    typeface = Typeface.createFromAsset(context.assets, "fonts/modern.otf")
                    setAutoSizeSpan()
                }
                TYPE_MODE_SAIGON -> {
                    typeface = Typeface.createFromAsset(context.assets, "fonts/saigon.otf")
                    setAutoSizeSpan()
                }
                TYPE_MODE_STRONG -> {
                    typeface = Typeface.createFromAsset(context.assets, "fonts/strong.otf")
                    setAutoSizeSpan()
                }
                TYPE_MODE_TYPE_WRITER -> {
                    typeface = Typeface.createFromAsset(context.assets, "fonts/type_writer.otf")
                }
                TYPE_MODE_CLASSIC -> {
                    typeface = Typeface.DEFAULT_BOLD
                }
            }
            isVerticalScaled = false
            calculateBoundary()
        }

    @BackgroundMode
    var textBackgroundMode = BACKGROUND_MODE_NONE
        set(value) {
            field = value
            calculateBoundary()
            invalidate()
        }

    override fun onTextChanged(
        text: CharSequence?,
        start: Int,
        lengthBefore: Int,
        lengthAfter: Int
    ) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
        if (lengthAfter == 0 && lengthBefore == 0) return
        path.reset()
        removeAutoSizeSpan()
        if (typeMode in arrayOf(TYPE_MODE_MODERN, TYPE_MODE_SAIGON, TYPE_MODE_STRONG)) {
            setAutoSizeSpan()
        }
        else if (typeMode == TYPE_MODE_CLASSIC) {
            scale()
        }
        verticalScale()
        calculateBoundary()
    }

    private fun setAutoSizeSpan() {
        if (text.isNullOrBlank()) return
        val textPaint = TextPaint()
        textPaint.textSize = MIN_SIZE.toFloat()
        textPaint.typeface = typeface
        val textLayoutAtMinSize = StaticLayout.Builder
            .obtain(
                text!!.take(text!!.length),
                0,
                text!!.length,
                textPaint,
                width - paddingLeft - paddingRight
            )
            .build()
        for (line: Int in 0 until textLayoutAtMinSize.lineCount) {
            val lineStart = textLayoutAtMinSize.getLineStart(line)
            val lineEnd = textLayoutAtMinSize.getLineEnd(line)
            val textSize = calculateTextSize(textLayoutAtMinSize, line)
            text?.setSpan(
                AbsoluteSizeSpan(textSize),
                lineStart,
                lineEnd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE
            )
        }
    }

    private fun removeAutoSizeSpan() {
        if (text != null) {
            for (span: ParcelableSpan in text!!.getSpans(
                0,
                text!!.length,
                ParcelableSpan::class.java
            )) {
                text!!.removeSpan(span)
            }
        }
    }

    private fun verticalScale() {
        var totalHeight = 0
        for (line: Int in 0 until layout.lineCount) {
            totalHeight += layout.getLineBottom(line) - layout.getLineTop(line)
        }
        totalHeight += paddingBottom + paddingTop
        if (totalHeight > availableHeight){
            if (totalHeight > screenSize.y) {
                val myLayoutParams = layoutParams
                myLayoutParams.height = totalHeight
                layoutParams = myLayoutParams
            }
            else {
                val myLayoutParams = layoutParams
                myLayoutParams.height = FrameLayout.LayoutParams.WRAP_CONTENT
                layoutParams = myLayoutParams
            }
            scaleX = availableHeight/totalHeight.toFloat()
            scaleY = availableHeight/totalHeight.toFloat()
            requestLayout()
        }
        else {
            val myLayoutParams = layoutParams
            myLayoutParams.height = FrameLayout.LayoutParams.WRAP_CONTENT
            layoutParams = myLayoutParams
            scaleX = 1f
            scaleY = 1f
        }
        isVerticalScaled = true
    }

    private fun calculateTextSize(textLayoutAtMinSize: Layout, line: Int): Int {
        var highSize = MAX_SIZE
        var lowSize = MIN_SIZE
        var currentSize = (lowSize + highSize) / 2
        val lineStart = textLayoutAtMinSize.getLineStart(line)
        val lineEnd = textLayoutAtMinSize.getLineEnd(line)
        var textLine = textLayoutAtMinSize.text.subSequence(lineStart, lineEnd)
        if (textLine.isEmpty()) return MIN_SIZE
        if (textLine[textLine.length - 1] == '\n') textLine =
            textLine.subSequence(0, textLine.length - 1)
        val linePaint = TextPaint()
        var lineWidth: Float
        linePaint.typeface = typeface
        while (currentSize > lowSize) {
            linePaint.textSize = currentSize.toFloat()
            lineWidth = linePaint.measureText(textLine.toString())
            if (lineWidth > textLayoutAtMinSize.width) {
                highSize = currentSize
            } else {
                lowSize = currentSize
            }
            currentSize = (lowSize + highSize) / 2
        }
        return currentSize
    }

    private fun getTextColor(): Int {
        return if (textBackgroundMode == BACKGROUND_MODE_NONE)
            mainColor
        else Color.WHITE
    }

    private fun getBackgroundColor(): Int {
        return if (textBackgroundMode == BACKGROUND_MODE_FILLED) {
            mainColor
        } else {
            Color.argb(
                70,
                Color.red(mainColor),
                Color.green(mainColor),
                Color.blue(mainColor)
            )
        }
    }

    override fun onDraw(canvas: Canvas) {
        if (!isVerticalScaled) {
            verticalScale()
            calculateBoundary()
        }
        canvas.drawPath(path, backgroundPaint)
        super.onDraw(canvas)
    }

    private fun calculateBoundary() {
        if (layout == null) return
        setTextColor(getTextColor())
        backgroundPaint.color = getBackgroundColor()
        padding = layout.height / layout.lineCount / 6f
        corner = layout.height / layout.lineCount / 3f
        path.reset()
        if (textBackgroundMode != BACKGROUND_MODE_NONE) {
            if (typeMode == TYPE_MODE_CLASSIC) initBoundary()
            for (i: Int in 0 until layout.lineCount) {
                if (layout.lineCount - 1 == i && layout.getLineWidth(i) == 0f) break
                if (typeMode == TYPE_MODE_CLASSIC) calculateRoundedBoundary(i)
                else calculateStraightBoundary(i)
            }
        }
    }

    private fun initBoundary() {
        val lines = ArrayList<LinesBoundary.LineBoundary>()
        for (line in 0 until layout.lineCount) {
            if (line == layout.lineCount - 1 && layout.getLineWidth(line) == 0f) break
            lines.add(
                LinesBoundary.LineBoundary(
                    layout.getLineLeft(line) - padding + paddingLeft,
                    layout.getLineTop(line).toFloat() + paddingTop,
                    layout.getLineRight(line) + padding + paddingLeft,
                    layout.getLineBottom(line).toFloat() + paddingTop,
                    corner
                )
            )
        }
        linesBoundary = LinesBoundary(lines)
    }

    private fun calculateRoundedBoundary(current: Int) {
        backgroundPaint.style = Paint.Style.FILL
        val left = linesBoundary.lines[current].left
        val right = linesBoundary.lines[current].right
        val top = linesBoundary.lines[current].top
        val bottom = linesBoundary.lines[current].bottom

        path.moveTo(left + corner, top)
        // Top line
        path.lineTo(right - corner, top)
        // Top right corner
        when {
            linesBoundary.isCurrentRightGreaterThanPreviousRight(current) -> {
                path.arcTo(
                    right - corner, top, right, top + corner,
                    -90f, 90f, false
                )
            }
            linesBoundary.isCurrentRightLessThanPreviousRight(current) -> {
                path.lineTo(right + corner, top)
                path.arcTo(
                    right, top, right + corner, top + corner,
                    -90f, -90f, false
                )
            }
            else -> {
                path.lineTo(right, top)
                path.lineTo(right, top + corner)
            }
        }
        // Right line
        path.lineTo(right, bottom - corner)
        // Bottom right corner
        when {
            linesBoundary.isCurrentRightGreaterThanNextRight(current) -> {
                path.arcTo(
                    right - corner, bottom - corner, right, bottom,
                    0f, 90f, false
                )
            }
            linesBoundary.isCurrentRightLessThanNextRight(current) -> {
                path.arcTo(
                    right, bottom - corner, right + corner, bottom,
                    180f, -90f, false
                )
            }
            else -> {
                path.lineTo(right, bottom)
            }
        }
        // Bottom line
        path.lineTo(left - corner, bottom)
        // Bottom left corner
        when {
            linesBoundary.isCurrentLeftLessThanNextLeft(current) -> {
                path.arcTo(
                    left, bottom - corner, left + corner, bottom,
                    90f, 90f, false
                )
            }
            linesBoundary.isCurrentLeftGreaterThanNextLeft(current) -> {
                path.lineTo(left - corner, bottom)
                path.arcTo(
                    left - corner, bottom - corner, left, bottom,
                    90f, -90f, false
                )
            }
            else -> {
                path.lineTo(left, bottom)
                path.lineTo(left, bottom - corner)
            }
        }
        // Left line
        path.lineTo(left, top - corner)
        // Top left corner
        when {
            linesBoundary.isCurrentLeftLessThanPreviousLeft(current) -> {
                path.arcTo(
                    left, top, left + corner, top + corner,
                    180f, 90f, false
                )
            }
            linesBoundary.isCurrentLeftGreaterThanPreviousLeft(current) -> {
                path.arcTo(
                    left - corner, top, left, top + corner,
                    0f, -90f, false
                )
            }
            else -> {
                path.lineTo(left, top)
                path.lineTo(left + corner, top)
            }
        }
    }

    private fun calculateStraightBoundary(current: Int) {
        backgroundPaint.style = Paint.Style.FILL
        val left = layout.getLineLeft(current) + paddingLeft - padding
        val right = layout.getLineRight(current) + paddingLeft + padding
        val top = layout.getLineTop(current)
        val bottom = layout.getLineBottom(current)
        path.moveTo(left, top.toFloat())
        // Top line
        path.lineTo(right, top.toFloat())
        // Right line
        path.lineTo(right, bottom.toFloat())
        // Bottom line
        path.lineTo(left, bottom.toFloat())
        // Left line
        path.lineTo(left, top.toFloat())
    }

    private fun scale(){
        removeAutoSizeSpan()
        text?.setSpan(
            RelativeSizeSpan(scaleInClassicMode),
            0,
            length(),
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        requestLayout()
        verticalScale()
    }

    override fun setGravity(gravity: Int) {
        super.setGravity(gravity)
        calculateBoundary()
    }
}