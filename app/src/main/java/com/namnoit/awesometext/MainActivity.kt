package com.namnoit.awesometext

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_controller.*


class MainActivity : AppCompatActivity(), KeyboardHeightObserver {
    private var typeMode = AwesomeEditText.TYPE_MODE_MODERN
    private var backgroundMode = AwesomeEditText.BACKGROUND_MODE_NONE
    private var alignment = Gravity.CENTER
    private var keyboardHeight = 0
    private lateinit var keyboardHeightProvider: KeyboardHeightProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        keyboardHeightProvider = KeyboardHeightProvider(this)
        mainLayout.post { keyboardHeightProvider.start() }
        awesomeEditText.textBackgroundMode = backgroundMode
        alignButton.visibility = View.GONE
        backgroundModeButton.visibility = View.GONE

        typeModeButton.setOnClickListener {
            when (typeMode) {
                AwesomeEditText.TYPE_MODE_MODERN -> {
                    setTypeModeSaigon()
                }
                AwesomeEditText.TYPE_MODE_SAIGON -> {
                    setTypeModeStrong()
                }
                AwesomeEditText.TYPE_MODE_STRONG -> {
                    setTypeModeTypeWriter()
                }
                AwesomeEditText.TYPE_MODE_TYPE_WRITER -> {
                    setTypeModeClassic()
                }
                else -> {
                    setTypeModeModern()
                }
            }
            awesomeEditText.typeMode = typeMode
        }

        backgroundModeButton.setOnClickListener {
            when (backgroundMode) {
                AwesomeEditText.BACKGROUND_MODE_NONE -> {
                    setFilledBackground()
                }
                AwesomeEditText.BACKGROUND_MODE_FILLED -> {
                    setTransparentBackground()
                }
                AwesomeEditText.BACKGROUND_MODE_TRANSPARENT -> {
                    setNoneBackground()
                }
            }
        }

        alignButton.setOnClickListener {
            when (alignment) {
                Gravity.CENTER -> {
                    alignment = Gravity.LEFT
                    alignButton.background = getDrawable(R.drawable.ic_align_left)
                }
                Gravity.LEFT -> {
                    alignment = Gravity.RIGHT
                    alignButton.background = getDrawable(R.drawable.ic_align_right)
                }
                else -> {
                    alignment = Gravity.CENTER
                    alignButton.background = getDrawable(R.drawable.ic_align_center)
                }
            }
            awesomeEditText.gravity = alignment
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar?,
                progress: Int,
                fromUser: Boolean
            ) {
                awesomeEditText.scaleInClassicMode = 0.3f + progress * 1.4f / 100

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

    }

    private fun setFilledBackground() {
        backgroundMode = AwesomeEditText.BACKGROUND_MODE_FILLED
        backgroundModeButton.backgroundMode = AwesomeEditText.BACKGROUND_MODE_FILLED
        awesomeEditText.textBackgroundMode = AwesomeEditText.BACKGROUND_MODE_FILLED
    }

    private fun setNoneBackground() {
        backgroundMode = AwesomeEditText.BACKGROUND_MODE_NONE
        backgroundModeButton.backgroundMode = AwesomeEditText.BACKGROUND_MODE_STROKE
        awesomeEditText.textBackgroundMode = AwesomeEditText.BACKGROUND_MODE_NONE
    }

    private fun setTransparentBackground() {
        backgroundMode = AwesomeEditText.BACKGROUND_MODE_TRANSPARENT
        backgroundModeButton.backgroundMode = AwesomeEditText.BACKGROUND_MODE_TRANSPARENT
        awesomeEditText.textBackgroundMode = AwesomeEditText.BACKGROUND_MODE_TRANSPARENT
    }

    private fun setTypeModeModern() {
        typeModeButton.text = resources.getString(R.string.modern)
        typeMode = AwesomeEditText.TYPE_MODE_MODERN
        backgroundMode = AwesomeEditText.BACKGROUND_MODE_NONE
        awesomeEditText.textBackgroundMode = backgroundMode
        backgroundModeButton.backgroundMode = AwesomeEditText.BACKGROUND_MODE_STROKE
        backgroundModeButton.visibility = View.GONE
        seekBar.visibility = View.GONE
        alignButton.visibility = View.GONE
        alignment = Gravity.CENTER
        awesomeEditText.gravity = alignment
        alignButton.background = getDrawable(R.drawable.ic_align_center)
    }

    private fun setTypeModeSaigon() {
        typeModeButton.text = resources.getString(R.string.saigon)
        typeMode = AwesomeEditText.TYPE_MODE_SAIGON
        backgroundMode = AwesomeEditText.BACKGROUND_MODE_NONE
        awesomeEditText.textBackgroundMode = backgroundMode
        backgroundModeButton.visibility = View.GONE
        alignButton.visibility = View.GONE
        alignment = Gravity.CENTER
        awesomeEditText.gravity = alignment
    }

    private fun setTypeModeStrong() {
        typeModeButton.text = resources.getString(R.string.strong)
        typeMode = AwesomeEditText.TYPE_MODE_STRONG
        backgroundMode = AwesomeEditText.BACKGROUND_MODE_NONE
        awesomeEditText.textBackgroundMode = backgroundMode
        backgroundModeButton.visibility = View.VISIBLE
        alignButton.visibility = View.GONE
        alignment = Gravity.CENTER
        awesomeEditText.gravity = alignment
    }

    private fun setTypeModeTypeWriter() {
        typeModeButton.text = resources.getString(R.string.type_writer)
        typeMode = AwesomeEditText.TYPE_MODE_TYPE_WRITER
        awesomeEditText.textSize = TYPE_WRITER_SIZE
        backgroundModeButton.visibility = View.VISIBLE
        alignButton.visibility = View.VISIBLE
        awesomeEditText.gravity = alignment
    }

    private fun setTypeModeClassic() {
        typeModeButton.text = resources.getString(R.string.classic)
        typeMode = AwesomeEditText.TYPE_MODE_CLASSIC
        seekBar.visibility = View.VISIBLE
        seekBar.progress = 50
        backgroundModeButton.visibility = View.VISIBLE
        awesomeEditText.scaleInClassicMode = 1f
        alignButton.visibility = View.VISIBLE
    }

    override fun onPause() {
        super.onPause()
        keyboardHeightProvider.observer = null
    }

    override fun onResume() {
        super.onResume()
        keyboardHeightProvider.observer = this
    }

    override fun onDestroy() {
        super.onDestroy()
        keyboardHeightProvider.close()
    }

    override fun onKeyboardHeightChanged(height: Int) {
        if (keyboardHeight != height) {
            awesomeEditText.topHeight = 2 * typeModeButton.measuredHeight
            awesomeEditText.keyboardHeight = height
            keyboardHeight = height
        }
    }
}
