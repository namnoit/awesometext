package com.namnoit.awesometext

import kotlin.math.abs

class LinesBoundary(val lines: ArrayList<LineBoundary>) {
    init {
        modifyBoundary()
    }

    private fun modifyBoundary(){
        if (lines.size == 0) return
        for (line in 0 until lines.size){
            // Current left little less than previous left
            if (line > 0 && lines[line].isLeftLessThan(lines[line - 1])) {
                lines[line].left = lines[line -1].left
            }
            // Current right little less than previous right
            if (line > 0 && lines[line].isRightLessThan(lines[line - 1])) {
                lines[line].right = lines[line -1].right
            }
            // Current left little less than next left
            if (line < lines.size - 1 && lines[line].isLeftLessThan(lines[line + 1])) {
                lines[line].left= lines[line + 1].left
                break
            }
            // Current right little less than next right
            if (line < lines.size - 1 && lines[line].isRightLessThan(lines[line + 1])) {
                lines[line].right= lines[line + 1].right
                break
            }
            if (line == lines.size - 1) return
        }
        modifyBoundary()
    }

    private fun isLastLine(current: Int) : Boolean {
        return current == lines.size - 1
    }

    private fun isFirstLine(current: Int) : Boolean {
        return current == 0
    }

    fun isCurrentLeftLessThanNextLeft(current: Int) : Boolean {
        return isLastLine(current) || lines[current].left < lines[current + 1].left
    }

    fun isCurrentRightLessThanNextRight(current: Int) : Boolean {
        return !isLastLine(current) && lines[current].right < lines[current + 1].right
    }

    fun isCurrentLeftGreaterThanNextLeft(current: Int) : Boolean {
        return !isLastLine(current) && lines[current].left > lines[current + 1].left
    }

    fun isCurrentRightGreaterThanNextRight(current: Int) : Boolean {
        return isLastLine(current) || lines[current].right > lines[current + 1].right
    }

    fun isCurrentLeftLessThanPreviousLeft(current: Int) : Boolean {
        return isFirstLine(current) || lines[current].left < lines[current - 1].left
    }

    fun isCurrentRightLessThanPreviousRight(current: Int) : Boolean {
        return !isFirstLine(current) && lines[current].right < lines[current - 1].right
    }

    fun isCurrentLeftGreaterThanPreviousLeft(current: Int) : Boolean {
        return !isFirstLine(current) && lines[current].left > lines[current - 1].left
    }

    fun isCurrentRightGreaterThanPreviousRight(current: Int) : Boolean {
        return isFirstLine(current) || lines[current].right > lines[current - 1].right
    }
    class LineBoundary(var left: Float,
                       var top: Float,
                       var right: Float,
                       var bottom: Float,
                       private var delta: Float)
    {
        fun isLeftLessThan(anotherLinesBoundary: LineBoundary): Boolean{
            return anotherLinesBoundary.left < left &&
                    abs(anotherLinesBoundary.left - left) < delta
        }

        fun isRightLessThan(anotherLineBoundary: LineBoundary): Boolean{
            return anotherLineBoundary.right > right &&
                    abs(right - anotherLineBoundary.right) < delta
        }
    }
}