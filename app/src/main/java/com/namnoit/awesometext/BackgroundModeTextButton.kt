package com.namnoit.awesometext

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet


class BackgroundModeTextButton : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) : this(context,null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defaultStyleAttributes: Int) :
            super(context, attributeSet, defaultStyleAttributes){
        text = "A"
        setTextColor(Color.WHITE)
        backgroundPaint.color = Color.WHITE
        backgroundPaint.strokeWidth = 5f
    }
    @AwesomeEditText.Companion.BackgroundMode
    var backgroundMode = AwesomeEditText.BACKGROUND_MODE_STROKE
        set(value) {
            field = value
            path.reset()
            when (field) {
                AwesomeEditText.BACKGROUND_MODE_STROKE -> {
                    setTextColor(Color.WHITE)
                    backgroundPaint.color = Color.WHITE
                }
                AwesomeEditText.BACKGROUND_MODE_FILLED -> {
                    setTextColor(Color.GRAY)
                    backgroundPaint.color = Color.WHITE
                }
                AwesomeEditText.BACKGROUND_MODE_TRANSPARENT -> {
                    setTextColor(Color.WHITE)
                    backgroundPaint.color = Color.parseColor("#B3FFFFFF")
                }
            }
            invalidate()
        }
    private var path = Path()
    private var backgroundPaint = Paint()
    private var padding = 10f
    private var corner = 2 * padding

    override fun onDraw(canvas: Canvas) {
        padding = textSize / 5f
        corner = 2 * padding
        drawBackground(canvas, backgroundMode != AwesomeEditText.BACKGROUND_MODE_STROKE)
        super.onDraw(canvas)
    }

    private fun drawBackground(canvas: Canvas, isFilled: Boolean){
        backgroundPaint.style = if (isFilled) Paint.Style.FILL else Paint.Style.STROKE
        if (layout.lineCount == 0) return
        val top = layout.getLineTop(0).toFloat() + paddingTop
        val bottom = layout.getLineBottom(0).toFloat() + paddingBottom
        val left = (layout.getLineRight(0) + layout.getLineLeft(0))/2f - (bottom - top)/2f + paddingLeft
        val right = (layout.getLineRight(0) + layout.getLineLeft(0))/2f + (bottom - top)/2f + paddingLeft
        path.moveTo(left + corner, top)
        // Top line
        path.lineTo(right - corner, top)
        // Top right corner
        path.arcTo(right - corner, top, right, top + corner,
            -90f, 90f, false
        )
        // Right line
        path.lineTo(right, bottom - corner)
        // Bottom right corner
        path.arcTo(right - corner, bottom - corner, right, bottom,
            0f, 90f, false
        )
        // Bottom line
        path.lineTo(left + corner, bottom)
        // Bottom left corner
        path.arcTo(left, bottom - corner, left + corner, bottom,
            90f, 90f, false
        )
        // Left line
        path.lineTo(left, top + corner)
        // Top left corner
        path.arcTo(left, top, left + corner, top + corner,
            180f, 90f, false
        )
        path.lineTo(left + corner, top)
        canvas.drawPath(path, backgroundPaint)
    }
}