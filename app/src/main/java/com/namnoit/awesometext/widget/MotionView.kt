package com.namnoit.awesometext.widget

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout
import com.namnoit.awesometext.gesture.DragGestureDetector
import com.namnoit.awesometext.gesture.RotationGestureDetector
import com.namnoit.awesometext.gesture.ScaleGestureDetector

class MotionView: FrameLayout,
    RotationGestureDetector.OnRotationGestureListener,
    ScaleGestureDetector.OnScaleGestureListener,
    DragGestureDetector.OnDragGestureListener {
    var entities: ArrayList<MotionEntity> = ArrayList()
        private set
    private var rotationGestureDetector: RotationGestureDetector? = null
    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var dragGestureDetector: DragGestureDetector? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defaultStyleAttributes: Int) :
            super(context, attributeSet, defaultStyleAttributes) {
        setWillNotDraw(false)
        rotationGestureDetector = RotationGestureDetector(this)
        scaleGestureDetector = ScaleGestureDetector(this)
        dragGestureDetector = DragGestureDetector(this)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            dragGestureDetector?.onTouchEvent(event)
            rotationGestureDetector?.onTouchEvent(event)
            scaleGestureDetector?.onTouchEvent(event)
        }
        return true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        drawAllEntities(canvas)
    }

    fun addEntity(entity: MotionEntity) {
        entity.moveToCanvasCenter()
        entities.add(entity)
        invalidate()
    }

    private fun drawAllEntities(canvas: Canvas?) {
        if (canvas != null) {
            entities.forEach { it.draw(canvas, null) }
        }
    }

    override fun onRotation(rotationDetector: RotationGestureDetector) {
        entities.forEach {
            it.layer.postRotate(-rotationDetector.angle)
            invalidate()
        }
    }

    override fun onScale(scaleGestureDetector: ScaleGestureDetector) {
        entities.forEach {
            it.layer.postScale(scaleGestureDetector.scale)
            invalidate()
        }
    }

    override fun onDrag(dragDetector: DragGestureDetector) {
        entities.forEach {
            it.layer.postTranslate(dragDetector.newX/width - dragDetector.x/width,
                dragDetector.newY/height - dragDetector.y/height)
            invalidate()
        }
    }
}