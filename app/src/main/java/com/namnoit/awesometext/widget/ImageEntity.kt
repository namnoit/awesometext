package com.namnoit.awesometext.widget

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import com.namnoit.awesometext.Layer
import kotlin.math.min

class ImageEntity(layer: Layer, private val bitmap: Bitmap, canvasWidth: Int, canvasHeight: Int):
    MotionEntity(layer, canvasWidth, canvasHeight) {

    init {
        val width = bitmap.width
        val height = bitmap.height

        val widthAspect = 1.0F * canvasWidth / width
        val heightAspect = 1.0F * canvasHeight / height
        // fit the smallest size
        holyScale = min(widthAspect, heightAspect);

        // initial position of the entity
        srcPoints[0] = 0f
        srcPoints[1] = 0f
        srcPoints[2] = width.toFloat()
        srcPoints[3] = 0f
        srcPoints[4] = width.toFloat()
        srcPoints[5] = height.toFloat()
        srcPoints[6] = 0f
        srcPoints[7] = height.toFloat()
        srcPoints[8] = 0f
        srcPoints[8] = 0f
    }

    public override fun drawContent(canvas: Canvas, drawingPaint: Paint?) {
        canvas.drawBitmap(bitmap, matrix, drawingPaint);
    }

    override fun getWidth(): Int {
        return bitmap.width;
    }

    override fun getHeight(): Int {
        return bitmap.height;
    }

    override fun release() {
        if (!bitmap.isRecycled) {
            bitmap.recycle();
        }
    }
}