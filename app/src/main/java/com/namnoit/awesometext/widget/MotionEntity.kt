package com.namnoit.awesometext.widget

import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.PointF
import com.namnoit.awesometext.Layer
import com.namnoit.awesometext.MathUtils


abstract class MotionEntity(
    val layer: Layer,
    protected var canvasWidth: Int,
    protected var canvasHeight: Int
) {

    /**
     * Transformation matrix for the entity
     */
    protected val matrix = Matrix()

    /**
     * maximum scale of the initial image, so that
     * the entity still fits within the parent canvas
     */
    protected var holyScale = 1f

    /**
     * Destination points of the entity
     * 5 points. Size of array - 10; Starting upper left corner, clockwise
     * last point is the same as first to close the circle
     * NOTE: saved as a field variable in order to avoid creating array in draw()-like methods
     */
    private val destPoints =
        FloatArray(10) // x0, y0, x1, y1, x2, y2, x3, y3, x0, y0

    /**
     * Initial points of the entity
     * @see .destPoints
     */
    protected val srcPoints =
        FloatArray(10) // x0, y0, x1, y1, x2, y2, x3, y3, x0, y0

    protected fun updateMatrix() { // init matrix to E - identity matrix
        matrix.reset()
        val topLeftX = layer.x * canvasWidth
        val topLeftY = layer.y * canvasHeight
        val centerX = topLeftX + getWidth() * holyScale * 0.5f
        val centerY = topLeftY + getHeight() * holyScale * 0.5f
        // calculate params
        val rotationInDegree = layer.rotationInDegrees
        val scaleX = layer.scale
        val scaleY = layer.scale
        // Applying transformations : L = S * R * T
        // Scale
        matrix.preScale(scaleX, scaleY, centerX, centerY)
        // Rotate
        matrix.preRotate(rotationInDegree, centerX, centerY)
        // Translate
        matrix.preTranslate(topLeftX, topLeftY)
        // Applying holy scale - S`, the result will be : L = S * R * T * S`
        matrix.preScale(holyScale, holyScale)
    }

    private fun absoluteCenter(): PointF {
        val topLeftX = layer.x * canvasWidth
        val topLeftY = layer.y * canvasHeight
        val centerX = topLeftX + getWidth() * holyScale * 0.5f
        val centerY = topLeftY + getHeight() * holyScale * 0.5f
        return PointF(centerX, centerY)
    }

    fun moveToCanvasCenter() {
        moveCenterTo(PointF(canvasWidth * 0.5f, canvasHeight * 0.5f))
    }

    private fun moveCenterTo(moveToCenter: PointF) {
        val currentCenter = absoluteCenter()
        layer.postTranslate(
            1.0f * (moveToCenter.x - currentCenter.x) / canvasWidth,
            1.0f * (moveToCenter.y - currentCenter.y) / canvasHeight
        )
    }

    private val pA = PointF()
    private val pB = PointF()
    private val pC = PointF()
    private val pD = PointF()

    /**
     * For more info:
     * [StackOverflow: How to check point is in rectangle](http://math.stackexchange.com/questions/190111/how-to-check-if-a-point-is-inside-a-rectangle)
     *
     * NOTE: it's easier to apply the same transformation matrix (calculated before) to the original source points, rather than
     * calculate the result points ourselves
     * @param point point
     * @return true if point (x, y) is inside the triangle
     */
    fun pointInLayerRect(point: PointF): Boolean {
        updateMatrix()
        // map rect vertices
        matrix.mapPoints(destPoints, srcPoints)
        pA.x = destPoints[0]
        pA.y = destPoints[1]
        pB.x = destPoints[2]
        pB.y = destPoints[3]
        pC.x = destPoints[4]
        pC.y = destPoints[5]
        pD.x = destPoints[6]
        pD.y = destPoints[7]
        return MathUtils.pointInTriangle(point, pA, pB, pC) ||
                MathUtils.pointInTriangle(point, pA, pD, pC)
    }

    /**
     * http://judepereira.com/blog/calculate-the-real-scale-factor-and-the-angle-of-rotation-from-an-android-matrix/
     *
     * @param canvas Canvas to draw
     * @param drawingPaint Paint to use during drawing
     */
    fun draw(canvas: Canvas, drawingPaint: Paint?) {
        updateMatrix()
        canvas.save()
        drawContent(canvas, drawingPaint)
        canvas.restore()
    }

    protected abstract fun drawContent(canvas: Canvas, drawingPaint: Paint?)

    abstract fun getWidth(): Int

    abstract fun getHeight(): Int

    abstract fun release();

    @Throws(Throwable::class)
    protected fun finalize() {
        try {
            release()
        } finally {

        }
    }
}