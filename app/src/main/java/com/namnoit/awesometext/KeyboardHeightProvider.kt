package com.namnoit.awesometext

import android.app.ActionBar
import android.app.Activity
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow

/**
 * This class is used to calculate soft keyboard height.
 * It will notify to MainActivity when keyboard height changed and height > 0
 *
 **/
class KeyboardHeightProvider(private val activity: Activity): PopupWindow(activity) {
    var observer: KeyboardHeightObserver? = null
    private var popupView: View
    private var parentView: View
    private var keyboardHeight = 0
    init {
        val layoutInflater =
            activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        popupView = layoutInflater.inflate(R.layout.layout_popup, null, false)
        contentView = popupView
        softInputMode = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE or
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
        inputMethodMode = INPUT_METHOD_NEEDED
        parentView = activity.findViewById(android.R.id.content)
        width = 0
        height = ActionBar.LayoutParams.MATCH_PARENT
        popupView.viewTreeObserver.addOnGlobalLayoutListener {
            handleOnGlobalLayout()
        }
    }

    private fun handleOnGlobalLayout() {
        val screenSize = Point()
        activity.windowManager.defaultDisplay.getSize(screenSize)
        val rect = Rect()
        popupView.getWindowVisibleDisplayFrame(rect)
        val keyboardHeight = screenSize.y - rect.bottom
        if (keyboardHeight == 0) {
            notifyKeyboardHeightChanged(0)
        }
        else {
            this.keyboardHeight = keyboardHeight
            notifyKeyboardHeightChanged(keyboardHeight)
        }
    }

    private fun notifyKeyboardHeightChanged(height: Int) {
        observer?.onKeyboardHeightChanged(height)
    }

    fun start() {
        if (!isShowing && parentView.windowToken != null) {
            setBackgroundDrawable(ColorDrawable(0))
            showAtLocation(parentView, Gravity.NO_GRAVITY, 0, 0)
        }
    }

    fun close() {
        observer = null
        dismiss()
    }
}