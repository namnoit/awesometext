package com.namnoit.awesometext

class Layer {
    var rotationInDegrees = 0f
    var scale = 1f
    /**
     * Top left X coordinate, relative to parent canvas
     */
    var x = 0f
    /**
     * Top left Y coordinate, relative to parent canvas
     */
    var y = 0f

    init {
        rotationInDegrees = 0.0f
        scale = 1.0f
        x = 0.0f
        y = 0.0f
    }

    fun postScale(scaleDiff: Float) {
        val newVal = scale * scaleDiff
        if (newVal in Limits.MIN_SCALE..Limits.MAX_SCALE) {
            scale = newVal
        }
    }

    fun postRotate(rotationInDegreesDiff: Float) {
        rotationInDegrees += rotationInDegreesDiff
        rotationInDegrees %= 360.0f
    }

    fun postTranslate(dx: Float, dy: Float) {
        x += dx
        y += dy
    }

    internal interface Limits {
        companion object {
            const val MIN_SCALE = 0.06f
            const val MAX_SCALE = 4.0f
            const val INITIAL_ENTITY_SCALE = 0.4f
        }
    }

}