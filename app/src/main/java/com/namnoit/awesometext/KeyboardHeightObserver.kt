package com.namnoit.awesometext

interface KeyboardHeightObserver {
    fun onKeyboardHeightChanged(height: Int)
}