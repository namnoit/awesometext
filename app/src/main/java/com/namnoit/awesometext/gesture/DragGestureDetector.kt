package com.namnoit.awesometext.gesture

import android.view.MotionEvent
import android.view.MotionEvent.INVALID_POINTER_ID


class DragGestureDetector(private val listener: OnDragGestureListener?) : GestureDetector {
    var x = 0f
    var y = 0f
    var newX = 0f
    var newY = 0f
    private var pointerId = INVALID_POINTER_ID

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                pointerId = event.getPointerId(event.actionIndex)
                newX = event.getX(event.findPointerIndex(pointerId))
                newY = event.getY(event.findPointerIndex(pointerId))
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
//                pointerId2 = event.getPointerId(event.actionIndex)
            }
            MotionEvent.ACTION_MOVE -> {
                x = newX
                y = newY
                newX = event.getX(event.findPointerIndex(pointerId))
                newY = event.getY(event.findPointerIndex(pointerId))
                listener?.onDrag(this)
            }
            MotionEvent.ACTION_UP -> pointerId = INVALID_POINTER_ID
            MotionEvent.ACTION_CANCEL -> {
                pointerId = INVALID_POINTER_ID
            }
        }
        return true
    }


    interface OnDragGestureListener {
        fun onDrag(dragDetector: DragGestureDetector)
    }
}