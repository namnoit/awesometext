package com.namnoit.awesometext.gesture

import android.view.MotionEvent
import kotlin.math.pow
import kotlin.math.sqrt

class ScaleGestureDetector(private val listener: OnScaleGestureListener?) : GestureDetector {
    private var pointerId1 = MotionEvent.INVALID_POINTER_ID
    private  var pointerId2 = MotionEvent.INVALID_POINTER_ID
    private var firstX = 0f
    private  var firstY = 0f
    private  var secondX = 0f
    private  var secondY = 0f
    var scale = 1f

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> pointerId1 = event.getPointerId(event.actionIndex)
            MotionEvent.ACTION_POINTER_DOWN -> {
                pointerId2 = event.getPointerId(event.actionIndex)
                firstX = event.getX(event.findPointerIndex(pointerId1))
                firstY = event.getY(event.findPointerIndex(pointerId1))
                secondX = event.getX(event.findPointerIndex(pointerId2))
                secondY = event.getY(event.findPointerIndex(pointerId2))
            }
            MotionEvent.ACTION_MOVE ->
                if (pointerId1 != MotionEvent.INVALID_POINTER_ID && pointerId2 != MotionEvent.INVALID_POINTER_ID) {
                    val newFirstX = event.getX(event.findPointerIndex(pointerId1))
                    val newFirstY = event.getY(event.findPointerIndex(pointerId1))
                    val newSecondX = event.getX(event.findPointerIndex(pointerId2))
                    val newSecondY = event.getY(event.findPointerIndex(pointerId2))
                    scale = getScaleFactor(
                        firstX,
                        firstY,
                        secondX,
                        secondY,
                        newFirstX,
                        newFirstY,
                        newSecondX,
                        newSecondY
                    )
                    firstX = newFirstX
                    firstY = newFirstY
                    secondX = newSecondX
                    secondY = newSecondY
                    listener?.onScale(this)
                }
            MotionEvent.ACTION_UP -> pointerId1 = MotionEvent.INVALID_POINTER_ID
            MotionEvent.ACTION_POINTER_UP -> pointerId2 = MotionEvent.INVALID_POINTER_ID
            MotionEvent.ACTION_CANCEL -> {
                pointerId1 = MotionEvent.INVALID_POINTER_ID
                pointerId2 = MotionEvent.INVALID_POINTER_ID
            }
        }
        return true
    }

    private fun getScaleFactor(firstX: Float,
                               firstY: Float,
                               secondX: Float,
                               secondY: Float,
                               newFirstX: Float,
                               newFirstY: Float,
                               newSecondX: Float,
                               newSecondY: Float) : Float
    {
        val currentDistance = sqrt((firstX - secondX).pow(2) + (firstY - secondY).pow(2))
        val newDistance = sqrt((newFirstX - newSecondX).pow(2) + (newFirstY - newSecondY).pow(2))
        return newDistance/currentDistance
    }

    interface OnScaleGestureListener {
        fun onScale(scaleGestureDetector: ScaleGestureDetector)
    }
}