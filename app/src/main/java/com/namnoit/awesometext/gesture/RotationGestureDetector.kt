package com.namnoit.awesometext.gesture

import android.view.MotionEvent
import android.view.MotionEvent.INVALID_POINTER_ID
import kotlin.math.atan2


class RotationGestureDetector(private val listener: OnRotationGestureListener?) : GestureDetector {
    private var firstX = 0f
    private var firstY = 0f
    private var secondX = 0f
    private var secondY = 0f
    private var pointerId1 = INVALID_POINTER_ID
    private var pointerId2 = INVALID_POINTER_ID
    var angle = 0f

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> pointerId1 = event.getPointerId(event.actionIndex)
            MotionEvent.ACTION_POINTER_DOWN -> {
                pointerId2 = event.getPointerId(event.actionIndex)
                firstX = event.getX(event.findPointerIndex(pointerId1))
                firstY = event.getY(event.findPointerIndex(pointerId1))
                secondX = event.getX(event.findPointerIndex(pointerId2))
                secondY = event.getY(event.findPointerIndex(pointerId2))
            }
            MotionEvent.ACTION_MOVE ->
                if (pointerId1 != INVALID_POINTER_ID && pointerId2 != INVALID_POINTER_ID) {
                    val newFirstX: Float = event.getX(event.findPointerIndex(pointerId1))
                    val newFirstY: Float = event.getY(event.findPointerIndex(pointerId1))
                    val newSecondX: Float = event.getX(event.findPointerIndex(pointerId2))
                    val newSecondY: Float = event.getY(event.findPointerIndex(pointerId2))
                    angle = angleBetweenLines(
                        firstX, firstY, secondX, secondY,
                        newFirstX, newFirstY, newSecondX, newSecondY
                    )
                    firstX = newFirstX
                    firstY = newFirstY
                    secondX = newSecondX
                    secondY = newSecondY
                    listener?.onRotation(this)
                }
            MotionEvent.ACTION_UP -> pointerId1 = INVALID_POINTER_ID
            MotionEvent.ACTION_POINTER_UP -> pointerId2 = INVALID_POINTER_ID
            MotionEvent.ACTION_CANCEL -> {
                pointerId1 = INVALID_POINTER_ID
                pointerId2 = INVALID_POINTER_ID
            }
        }
        return true
    }

    private fun angleBetweenLines(
        fX: Float,
        fY: Float,
        sX: Float,
        sY: Float,
        nfX: Float,
        nfY: Float,
        nsX: Float,
        nsY: Float
    ): Float {
        val angle1 =
            atan2((fY - sY).toDouble(), (fX - sX).toDouble()).toFloat()
        val angle2 =
            atan2((nfY - nsY).toDouble(), (nfX - nsX).toDouble()).toFloat()
        var angle =
            Math.toDegrees(angle1 - angle2.toDouble()).toFloat()
        if (angle < -180f) angle += 360.0f
        if (angle > 180f) angle -= 360.0f
        return angle
    }

    interface OnRotationGestureListener {
        fun onRotation(rotationDetector: RotationGestureDetector)
    }
}