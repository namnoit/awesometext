package com.namnoit.awesometext.gesture

import android.view.MotionEvent

interface GestureDetector {
    fun onTouchEvent(event: MotionEvent): Boolean
}